import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import {Button,Image} from '@tarojs/components'
import Taro from '@tarojs/taro'
import utils from '../../untils'
//引入api
// import api from "../../services/api/user";



export default class Index extends Component {
  state={
    imgList:[]
  }
  toDeatilPage (index){
    console.log("跳转")
    Taro.navigateTo({
      url: `/pages/Detail/index?id=${index}`,
    })
  }
  // toLogin(){
  //   import login from "../../services/api/login"
  //   console.log("去登陆",login)
  //   // login.gotologin()
  //   // let params={
  //   //   userName:'测试1',
  //   //   password:'123456'
  //   // };
  //   // api.addAssetsRepair(params).then((res) => {
  //   //   console.log(res)
  //   // });
  // }
  componentWillMount () {
    let imgList= []
    console.log("将要渲染")
    for(let i =0; i<20 ; i++){
      imgList.push({
        id:i,
        url:require('../../img/641.jpg')
      })
    }
    this.setState({
      imgList:imgList
    },()=>{
      console.log(this.state.imgList)
    })
   }

  componentDidMount () { 
    console.log("完成渲染")
  }

  componentWillUnmount () { 
    console.log("componentWillUnmount --- 将要注销")
  }

  componentDidShow () {
    console.log("componentDidShow --- 页面显示")
  }

  componentDidHide () { 
    console.log("componentDidHide --- 隐藏")
  }

  componentWillUpdate(){
    console.log("componentWillUpdate --- state数据更新之前")
    
  }
  componentDidUpdate(){
    console.log("componentDidUpdate --- state数据更新之后")
    
  }

  shouldComponentUpdate(nextProps, nextState){
    //控制此次setstate是否要进行render的调用
    // 减少没必要的渲染，数据没有更新就不进行渲染
    console.log("shouldComponentUpdate --- ",nextState)
    return true;
  }
  render () {
    console.log("render")
    return (
      <View className='imgList'>
        {/* <Button onClick={()=>{this.toLogin()}}>登陆</Button> */}
        {
          this.state.imgList.map((item,index)=>{
            return<View key={index} className="listItem" onClick={()=>{this.toDeatilPage(index)}}>
              <Image src={item.url} mode="scaleToFill"></Image>
            </View>
          })
        }
        
      </View>
    )
  }
}
