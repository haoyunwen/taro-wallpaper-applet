import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import { Button, Image } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './index.scss'
export default class detail extends Component {
  state = {
    width: '',
    height: '',
  }
  goBack() {
    Taro.navigateBack()
  }
  onShareTimeline () {
    console.log('onShareTimeline')
    return {}
  }
  onShareAppMessage(){
    return{
      title:'分享给好友的内容',
      // imagerUrl:imgSrc,
      path:'/pages/detail/index'
    }
  }
  handleDownLoad(){
    console.log("下载")
    Taro.downloadFile({
      url: '../../img/123.jpg', //仅为示例，并非真实的资源
      success: function (res) {
        // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
        if (res.statusCode === 200) {
          Taro.playVoice({
            filePath: res.tempFilePath
          })
        }
      }
    })
  }
  componentWillMount() {
    console.log("将要渲染")
    Taro.getSystemInfo({
      success: (res) => {
        this.setState({
          width: res.windowWidth + 'px',
          height: res.windowHeight + 'px'
        })
      }
    })
  }

  componentDidMount() {

  }

  componentWillUnmount() {
    console.log("componentWillUnmount --- 将要注销")
  }

  componentDidShow() {
    console.log("componentDidShow --- 页面显示")
  }

  componentDidHide() {
    console.log("componentDidHide --- 隐藏")
  }

  componentWillUpdate() {
    console.log("componentWillUpdate --- state数据更新之前")

  }
  componentDidUpdate() {
    console.log("componentDidUpdate --- state数据更新之后")

  }
  render() {
    console.log("render")
    return (
      <View id='detailPage'>
        <Image style={{ width: this.state.width, height: this.state.height }} src={require('../../img/123.jpg')}></Image>
        <View className='bottomView'>
          <View className='icoItem' onClick={() => { this.goBack() }}>
            <View className='at-icon at-icon-chevron-left'></View>
            返回
          </View>
          <View className='icoItem' >
            <View className='at-icon at-icon-star'></View>
            收藏
          </View>
          <View className='icoItem' onClick={()=>{this.handleDownLoad()}}>
            <View className='at-icon at-icon-share-2'></View>
            下载
          </View>
          <View className='icoItem'>
            <View className='at-icon at-icon-share'></View>
            分享
          </View>
        </View>
      </View>
    )
  }
}
