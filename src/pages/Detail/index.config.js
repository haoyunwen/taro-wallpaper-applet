export default definePageConfig({
  navigationBarTitleText: '详情',
  navigationStyle:'custom' ,
   // 当 `onShareAppMessage` 没有触发时，可以尝试配置此选项
   enableShareAppMessage: true,
   // 当 `onShareTimeline` 没有触发时，可以尝试配置此选项
   enableShareTimeline: true
})
