import { Component } from 'react'
import { View, Text,Image } from '@tarojs/components'
import {Current} from '@tarojs/taro'
import { AtAvatar, AtList, AtListItem   } from 'taro-ui'
import  './index.scss'
import login from "../../services/api/login"
export default class MyPage extends Component {
  toLogin(){
    
    console.log("去登陆",login)
    // login.gotologin()
    // let params={
    //   userName:'测试1',
    //   password:'123456'
    // };
    // api.addAssetsRepair(params).then((res) => {
    //   console.log(res)
    // });
  }
  componentWillMount () {
    console.log("参数",Current.router.params)
    
   }

  componentDidMount () { 
    console.log("完成渲染")
    
  }

  componentWillUnmount () { 
    console.log("componentWillUnmount --- 将要注销")
  }

  componentDidShow () {
    console.log("componentDidShow --- 页面显示")
  }

  componentDidHide () { 
    console.log("componentDidHide --- 隐藏")
  }

  componentWillUpdate(){
    console.log("componentWillUpdate --- state数据更新之前")
    
  }
  componentDidUpdate(){
    console.log("componentDidUpdate --- state数据更新之后")
    
  }

  // shouldComponentUpdate(nextProps, nextState){
  //   //控制此次setstate是否要进行render的调用
  //   // 减少没必要的渲染，数据没有更新就不进行渲染
  //   console.log("shouldComponentUpdate --- ",nextState)
  //   return true;
  // }
  render () {
    console.log("render")
    return (
      <View className='index'>
         <View className='userMsgBg'>
            <Image src={require('../../img/641.jpg')}></Image>
          </View>
        <View className='userMsg'>
          <AtAvatar onClink={()=>{this.toLogin()}} circle image='https://jdc.jd.com/img/200'></AtAvatar>
          <View className='username'>
            <text>用户名:</text>
            <text>vip</text>
          </View>
        </View>
        <AtList>
          <AtListItem
            title='我的收藏'
            arrow='right'
            iconInfo={{ size: 25, color: '#78A4FA', value: 'calendar', }}
          />
          <AtListItem
            title='我的下载'
            arrow='right'
            iconInfo={{ size: 25, color: '#FF4949', value: 'download', }}
          />
          <AtListItem
            title='退出登录'
            arrow='right'
            iconInfo={{ size: 25, color: '#FF4949', value: 'external-link            ', }}
          />
          <AtListItem
            title='更多'
            arrow='right'
            iconInfo={{ size: 25, color: '#FF4949', value: 'add-circle', }}
          />
        </AtList>
      </View>
    )
  }
}
