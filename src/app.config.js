export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/myPage/index',
    'pages/Detail/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
   tabBar: {
      color: 'rgba(68, 68, 68, 1)',
      selectedColor: '#d81e06',
      backgroundColor: 'white',
      list: [
        {
          pagePath: 'pages/index/index',
          text: '首页',
          iconPath: './img/ic_home_normal.png',
          selectedIconPath: './img/ic_home_selected.png'
        },
        {
          pagePath: 'pages/myPage/index',
          text: '我的',
          iconPath: './img/ic_me_normal.png',
          selectedIconPath: './img/ic_me_selected.png'
        }]
    }
})
